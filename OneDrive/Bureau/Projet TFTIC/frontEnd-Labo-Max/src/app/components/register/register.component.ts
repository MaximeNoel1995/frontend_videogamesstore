import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../../services/register.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RegisterModel } from 'src/app/models/registerModel';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user = new RegisterModel

  constructor(
    private registerService: RegisterService,
    private router: Router
  ) 
  { }

  ngOnInit() {
  }

  addUser() {
    this.registerService.createUser(this.user).subscribe(data => {
      console.log(data),
      alert("Register Completed Succesfully !!"),
      this.router.navigate([''])
    }) 
  }
}
