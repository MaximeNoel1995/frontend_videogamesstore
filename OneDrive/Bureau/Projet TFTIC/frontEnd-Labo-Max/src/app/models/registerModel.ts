export class RegisterModel {
    email: string;
    username: string;
    address: string;
    name: string;
    firstName: string;
    password: string;
 }